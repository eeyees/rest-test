# rest-test

## 가상 환경 설정

__windows 기준__

1. 프로젝트 폴더 만들기 및 이동

    C:\>git clone https://gitlab.com/eeyees/rest-test.git

2. 가상 환경 설정

    C:\rest-test\>virtualenv venv

3. 가상 환경 실행

    C:\rest-test\>.\venv\Script\activate

3. 의존성 라이브러리 설치

    (venv) C:\rest-test>pip install -r requirement.txt

    (pip upgrade가 필요하다면 진행)

4. 플라스크 실행

    (venv) C:\rest-test>flask run


__브라우저에서 rest api 확인 방법__

1. localhost:5000/api/product/{id}

2. localhost:5000/api/product_meta/{id}

