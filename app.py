from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restless import APIManager

app = Flask(__name__)

app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://jung:jung@127.0.0.1:5432/woom'

db = SQLAlchemy(app)

class Product_Meta(db.Model):
    __tablename__ = 'product_meta'
    id=db.Column(db.Integer, db.Sequence('product_meta_seq'), primary_key=True)
    meta_key=db.Column('meta_key', db.Text)
    meta_value=db.Column('meta_value', db.Text)
    create_date=db.Column('create_date', db.Date)
    modified_date=db.Column('modified_date', db.Date)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)

class Product(db.Model):
    __tablename__ = 'product'
    id=db.Column(db.Integer, db.Sequence('product_id_seq'), primary_key=True)
    title=db.Column('title', db.Text)
    author=db.Column('author', db.Text)
    product_content=db.Column('product_content', db.Text)
    product_thumb=db.Column('product_thumb', db.Text)
    product_status=db.Column('product_status', db.Text)
    create_date=db.Column('create_date', db.Date)
    modified_date=db.Column('modified_date', db.Date)
    product_metas = db.relationship('Product_Meta', backref=db.backref('metas', lazy=True)) 

db.create_all()

manager = APIManager(app, flask_sqlalchemy_db=db)
manager.create_api(Product)
manager.create_api(Product_Meta)

app.run()